package com.example.app;

import android.app.Activity;
import android.location.LocationManager;
import android.os.Bundle;

import javax.inject.Inject;

public class MainActivity extends Activity {

    @Inject
    LocationManager locationManager;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // After the super.onCreate call returns we are guaranteed our injections are available.

        // TODO do something with the injected dependencies here!
    }

}
