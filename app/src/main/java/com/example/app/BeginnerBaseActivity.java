package com.example.app;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by raulete on 12/02/14.
 */
public abstract class BeginnerBaseActivity extends Activity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Perform injection so that when this call returns all dependencies will be available for use.
        ((BeginnerApplication) getApplication()).inject(this);
    }
}
